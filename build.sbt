scalaVersion in ThisBuild := "2.13.1"

// https://docs.scala-lang.org/overviews/compiler-options/index.html
val scalacCompileOpts =  Seq(
  "-feature",
  "-unchecked",
  // "-deprecation:false", // uncomment if you *must* use deprecated apis
  "-Xfatal-warnings",
  "-Ywarn-value-discard",
)

lazy val akkaVersion = "2.6.41"
lazy val akkaHttpVersion = "10.1.11"
lazy val circeVersion = "0.13.0"

lazy val proxy = project
  .settings(
    libraryDependencies ++= Seq(
      "io.circe" %% "circe-core" % circeVersion,
      "io.circe" %% "circe-generic" % circeVersion,
      "io.circe" %% "circe-parser" % circeVersion,
      "io.circe" %% "circe-optics" % circeVersion,
      "com.typesafe.akka" %% "akka-http" % akkaHttpVersion,
      "com.typesafe.akka" %% "akka-stream" % akkaVersion,
      "com.typesafe.akka" %% "akka-actor-typed" % akkaVersion,
      "com.typesafe.akka" %% "akka-actor" % akkaVersion,
      "com.typesafe.akka" %% "akka-http-spray-json" % akkaHttpVersion,
      "de.heikoseeberger" %% "akka-http-circe" % "1.31.0",
      "ch.qos.logback" % "logback-classic" % "1.2.3",
      "com.github.julien-truffaut" %%  "monocle-core"  % "2.0.4",

      "com.typesafe.akka" %% "akka-testkit" % akkaVersion % Test,
      "com.typesafe.akka" %% "akka-http-testkit" % akkaVersion % Test,
      "org.scalatest" %% "scalatest" % "3.0.8" % Test,
      "org.scalamock" %% "scalamock" % "4.4.0" % Test
    ),
    scalacOptions in(Compile, compile) ++= scalacCompileOpts
  )

lazy val backend = project
  .settings(
    libraryDependencies ++= Seq(
      "io.circe" %% "circe-core" % "0.13.0",
      "io.circe" %% "circe-generic" % "0.13.0",
      "io.circe" %% "circe-parser" % "0.13.0",
      "com.typesafe.akka" %% "akka-http" % "10.1.11",
      "com.typesafe.akka" %% "akka-stream" % "2.6.4",
      "com.typesafe.akka" %% "akka-actor-typed" % "2.6.4",
      "com.typesafe.akka" %% "akka-actor" % "2.6.4",
      "com.typesafe.akka" %% "akka-http-spray-json" % "10.1.11",
      "de.heikoseeberger" %% "akka-http-circe" % "1.31.0",
      "ch.qos.logback" % "logback-classic" % "1.2.3",

      "com.typesafe.akka" %% "akka-testkit" % "2.6.4" % Test,
      "com.typesafe.akka" %% "akka-http-testkit" % "10.1.11" % Test,
      "org.scalatest" %% "scalatest" % "3.0.8" % Test,
      "org.scalamock" %% "scalamock" % "4.4.0" % Test
    ),
    scalacOptions in(Compile, compile) ++= scalacCompileOpts
  )